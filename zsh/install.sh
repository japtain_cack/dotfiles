#!/usr/bin/env bash
cwd=$(dirname "$(readlink -f "$0")")
parentdir="$(dirname "${cwd}")"
now=$(date '+%Y-%m-%dT%H-%M-%S')
GITDIR="${HOME}/git"
ZSHDIR="${HOME}/.zsh"
CACHEDIR="${ZSHDIR}/cache"        # zsh completions chache
ZGENDIR="${ZSHDIR}/zgen"

# Colors
RESET='\033[0m'
RED='\033[00;31m'
GREEN='\033[00;32m'
YELLOW='\033[00;33m'
BLUE='\033[00;34m'
PURPLE='\033[00;35m'
CYAN='\033[00;36m'
LIGHTGRAY='\033[00;37m'
LRED='\033[01;31m'
LGREEN='\033[01;32m'
LYELLOW='\033[01;33m'
LBLUE='\033[01;34m'
LPURPLE='\033[01;35m'
LCYAN='\033[01;36m'
WHITE='\033[01;37m'


################################
# zsh configurations
################################
if [ ! -d $ZSHDIR ]; then         # create $ZSHDIR if it does not exist
  mkdir -p $ZSHDIR
fi
if [ ! -d $CACHEDIR ]; then       # create $CACHEDIR if it does not exist
  mkdir -p $CACHEDIR
fi
if [ ! -d $CACHEDIR ]; then       # create $CACHEDIR if it does not exist
  mkdir -p $CACHEDIR
fi

# Install/Update the Zgen plugin manager
if [ -d ${ZGENDIR}/.git ]; then
  cd ${ZGENDIR}
  git pull
  cd "${cwd}"
  echo -e "${LRED}Please run:\n\tzgen reset\nOtherwise any new/removed zsh options may not be applied.${RESET}"
else
  git clone https://github.com/tarjoilija/zgen.git "${ZGENDIR}"
fi
echo

# Install the .zshrc file and create a backup if one already exists
if [ -f ${HOME}/.zshrc ]; then
  if [ ! -L ${HOME}/.zshrc ]; then
    # backup existing config
    mkdir -p ${ZSHDIR}/backups
    mv -v ${HOME}/.zshrc ${ZSHDIR}/backups/.zshrc_${now}
    echo "Existing .zshrc found in user home. Backup created: ${ZSHDIR}/backups/.zshrc_${now}"

    ln -fs ${cwd}/files/zshrc ${HOME}/.zshrc
  fi
else
  ln -fs ${cwd}/files/zshrc ${HOME}/.zshrc
fi
touch ${HOME}/.zshrc_local
touch ${HOME}/.zshrc_env
echo

# Install the .bash_aliases file and create a backup if one already exists
if [ -f ${HOME}/.bash_aliases ]; then
  if [ ! -L ${HOME}/.bash_aliases ]; then
    # backup existing config
    mkdir -p ${ZSHDIR}/backups
    mv -v ${HOME}/.bash_aliases ${ZSHDIR}/backups/.bash_aliases_${now}
    echo "Existing .bash_aliases found in user home. Backup created: ${ZSHDIR}/backups/.bash_aliases_${now}"

    ln -fs ${parentdir}/profile/bash_aliases ${HOME}/.bash_aliases
  fi
else
  ln -fs ${parentdir}/profile/bash_aliases ${HOME}/.bash_aliases
fi
touch ${HOME}/.bash_aliases_local
echo

# Install the .p10k.zsh file and create a backup if one already exists
if [ -f ${HOME}/.p10k.zsh ]; then
  if [ ! -L ${HOME}/.p10k.zsh ]; then
    # backup existing config
    mkdir -p ${ZSHDIR}/backups
    mv -v ${HOME}/.p10k.zsh ${ZSHDIR}/backups/.p10k.zsh_${now}
    echo "Existing .p10k.zsh found in user home. Backup created: ${ZSHDIR}/backups/.p10k.zsh_${now}"

    ln -fs ${cwd}/files/.p10k.zsh ${HOME}/.p10k.zsh
  fi
else
  ln -fs ${cwd}/files/.p10k.zsh ${HOME}/.p10k.zsh
fi
touch ${HOME}/.bash_aliases_local
echo


echo -e "${LRED}Please run:\n\tcompaudit | xargs chmod g-w,o-w\nThis will correct any file permission issues for completion directories.${RESET}"

echo zsh installer complete

