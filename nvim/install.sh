#!/usr/bin/env bash
cwd=$(dirname "$(readlink -f "$0")")
now=$(date '+%Y-%m-%dT%H-%M-%S')
VIMDIR="${HOME}/.vim"
NVIMDIR="${HOME}/.config/nvim"
GITDIR="${VIMDIR}/bundle"

# Colors
RESET='\033[0m'
RED='\033[00;31m'
GREEN='\033[00;32m'
YELLOW='\033[00;33m'
BLUE='\033[00;34m'
PURPLE='\033[00;35m'
CYAN='\033[00;36m'
LIGHTGRAY='\033[00;37m'
LRED='\033[01;31m'
LGREEN='\033[01;32m'
LYELLOW='\033[01;33m'
LBLUE='\033[01;34m'
LPURPLE='\033[01;35m'
LCYAN='\033[01;36m'
WHITE='\033[01;37m'

if [ ! -d ${HOME}/.config ]; then
  mkdir -v ${HOME}/.config
fi

if [ ! -d ${VIMDIR} ]; then
  mkdir -v ${VIMDIR}
fi

if [ ! -d ${NVIMDIR} ]; then
  mkdir -vp ${NVIMDIR}
fi

if [ ! -f ${HOME}/.config/nvim/init.vim ]; then

cat <<EOF > ${HOME}/.config/nvim/init.vim
set runtimepath^=${VIMDIR} runtimepath+=${VIMDIR}/after
let &packpath = &runtimepath
source ${HOME}/.vimrc

if !empty(glob("${HOME}/.nvimrc.local"))
  source ${HOME}/.nvimrc.local
endif
EOF

fi

echo nvim installer complete

