sudo apt-get install openssh-client autossh ssh-askpass
cd /tmp
curl -L https://api.github.com/repos/StanfordSNR/guardian-agent/releases/latest | grep browser_download_url | grep 'linux' | cut -d'"' -f 4 | xargs curl -Ls | tar xzv
sudo cp sga_linux_amd64/* /usr/local/bin
rm -rfv sga_linux_amd64
