#!/usr/bin/env bash
cwd=$(dirname "$(readlink -f "$0")")
now=$(date '+%Y-%m-%dT%H-%M-%S')

# Colors
RESET='\033[0m'
RED='\033[00;31m'
GREEN='\033[00;32m'
YELLOW='\033[00;33m'
BLUE='\033[00;34m'
PURPLE='\033[00;35m'
CYAN='\033[00;36m'
LIGHTGRAY='\033[00;37m'
LRED='\033[01;31m'
LGREEN='\033[01;32m'
LYELLOW='\033[01;33m'
LBLUE='\033[01;34m'
LPURPLE='\033[01;35m'
LCYAN='\033[01;36m'
WHITE='\033[01;37m'


if [ $(command -v zsh) ]; then
  ./zsh/install.sh
fi

if [ $(command -v tmux) ]; then
  ./tmux/install.sh
fi

if [ $(command -v vim) ]; then
  ./vim/install.sh
fi

if [ $(command -v nvim) ]; then
  ./nvim/install.sh
fi

./fzf/install.sh

echo Installation complete

